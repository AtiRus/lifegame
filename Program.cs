﻿using System;
using System.Linq;
using System.Threading;
using System.IO;

namespace LifeGame
{
    class Program
    {
        static class LifeGame
        {

            private static int CountNeighbourhoods(int[,] a, int x, int y)
            {
                int m = a.GetLength(0) - 1;
                int n = a.GetLength(1) - 1;
                int c = 0;
                if (x != 0 && y != 0) { c += a[x - 1, y - 1]; }
                if (x != 0) { c += a[x - 1, y]; }
                if (x != 0 && y != n) { c += a[x - 1, y + 1]; }
                if (y != 0) { c += a[x, y - 1]; }
                if (y != n) { c += a[x, y + 1]; }
                if (x != m && y != 0) { c += a[x + 1, y - 1]; }
                if (x != m) { c += a[x + 1, y]; }
                if (x != m && y != n) { c += a[x + 1, y + 1]; }
                if (c == 4) { return 4; }
                return c;
            }

            private static void NextTurn(int[,] a, int[,] b)
            {
                int m = a.GetLength(0);
                int n = a.GetLength(1);
                for (int i = 0; i < m; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        int c = CountNeighbourhoods(a, i, j);
                        b[i, j] = (c == 3 || (c == 2 && a[i, j] != 0)) ? 1 : 0;
                    }
                }
            }
            private static void LifeGameGenerate(int l, int w)
            {
                Random rnd = new Random();
                int[,] a = new int[l, w];
                int[,] b = new int[l, w];

                do
                {
                    for (int i = 0; i < l; i++)
                    {
                        for (int j = 0; j < w; j++)
                        {
                            a[i, j] = rnd.Next(0, 2);
                        }
                    }

                    Console.WriteLine("\n\n" + "LifeGame Table:");
                    for (int i = 0; i < l; i++)
                    {
                        for (int j = 0; j < w; j++)
                        {
                            Console.Write(a[i, j] == 0 ? " 0 " : " 1 ");
                        }
                        Console.WriteLine();
                    }
                    NextTurn(a, b);
                    Swap(a, b);
                    Thread.Sleep(1000);
                } while (true);
            }

            static void Swap(int[,] a, int[,] b)
            {
                int[,] temp = a;
                a = b;
                b = temp;
            }
            public static void Main(string[] args)
            {
                string path = @"C:\Users\AtiRu\source\repos\LifeGame\LifeGame\TestMap.txt";
                Console.WriteLine("Введите 1, если необходимо получить данные из файла" + "\n"
                + "Введите 2, если необходимо ввести данные вручную:");
                try
                {
                    int choose = Int32.Parse(Console.ReadLine());
                    switch (choose)
                    {
                        case (1):
                            string[] rows = File.ReadAllLines(path);
                            int[,] arr = new int[rows.Length, rows[0].Split(" ").Length];
                            for (int i = 0; i < rows.Length; ++i)
                            {
                                string[] tmp = rows[i].Split(" ");
                                for (int j = 0; j < rows[0].Split().Length; ++j)
                                {
                                    arr[i, j] = int.Parse(tmp[j]);
                                }
                            }
                            for (int i = 0; i < arr.GetLength(0); ++i)
                            {
                                for (int j = 0; j < arr.GetLength(1); ++j)
                                {
                                    Console.Write(arr[i, j] + " ");
                                }
                                Console.WriteLine();
                            }
                            LifeGameGenerate(arr.GetLength(0), arr.GetLength(1));
                            break;
                        case (2):
                            Console.WriteLine("Введите длину таблицы:");
                            int l = Int32.Parse(Console.ReadLine());
                            Console.WriteLine("Введите ширину таблицы:");
                            int w = Int32.Parse(Console.ReadLine());
                            LifeGameGenerate(l, w);
                            break;
                        default:
                            throw new ArgumentException();
                    }
                } catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}
